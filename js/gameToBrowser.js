"use strict";

function createGrid(game, difficulty) {

    var gridH, gridV;
    var mineCount;

    if (difficulty === 'easy') {
        gridH = 10;
        gridV = 8;
        mineCount = 10;
    }
    else if (difficulty === 'medium') {
        gridH = 18;
        gridV = 14;
        mineCount = 40;
    }
    else if (difficulty === 'hard') {
        gridH = 24;
        gridV = 20;
        mineCount = 99;
    }

    game.init(gridV, gridH, mineCount);
    console.log(game.getRendering().join("\n"));
    console.log(game.getStatus());
    var gridOutside = document.getElementById('gridLocation');
    $('#gridLocation').empty();
    gridOutside.classList.add('grid-container');
    console.log('repeat(' + gridH + ', ' + gridOutside.offsetWidth / gridH + 'px)');
    gridOutside.style.height = (gridV * (gridOutside.offsetWidth / gridH)) + 'px';
    gridOutside.style.display = 'grid';
    gridOutside.style.gridTemplateColumns = 'repeat(' + gridH + ', ' + gridOutside.offsetWidth / gridH + 'px)';
    gridOutside.style.gridTemplateRows = 'repeat(' + gridV + ', ' + gridOutside.offsetWidth / gridH + 'px)';
    gridOutside.classList.add('grid-container');
    gridOutside.style.margin = 'auto'

    for (var i = 0; i < gridV; i++) {
        for (var j = 0; j < gridH; j++) {
            var gridElement = document.createElement("div");
            gridElement.setAttribute('x', i);
            gridElement.setAttribute('y', j);
            gridElement.setAttribute('flag', false);
            gridElement.classList.add('grid-item');
            gridElement.classList.add('hidden');
            gridOutside.appendChild(gridElement);
        }
    }
    console.log("Grid Created")
}

function makeMove(game, gridX, gridY) {
    game.uncover(gridX, gridY);
    console.log(game.getRendering().join("\n"));
}

function markFlag(game, element, flag) {
    if (element.hasClass('hidden')) {
        if (flag === 'true') {
            element.attr('flag', 'false');
        }
        else {
            element.attr('flag', 'true');
        }
        game.mark(Number(element.attr('x')), Number(element.attr('y')));
    }
    console.log(game.getRendering().join("\n"));
}

function checkStatus(status) {
    $('#flagCount').text("Flag Count: " + status.nmarked);
    $('#mineNum').text("Mine Count: " + status.nmines);

    if (status.done === true && status.exploded === false) {
        return 'win';
    }
    else if (status.done === true && status.exploded === true) {
        return 'lose';
    }
    else {
        return 'none';
    }
}

function winScreen(status) {
    var state = checkStatus(status);
    if (state === 'win') {
        alert("All Mines Found!");
        $("#winMessage").text("Congratulations! You have won! Click a difficulty to restart!");
    }
    else if (state === 'lose') {
        alert("BOOOOOOOOOOOOM");
        $("#winMessage").text("Too Bad! You lost! Click a difficulty to restart!")
    }
}

function updateGrid(game) {
    var numOfFlags = 0;
    var currentGrid = game.getMinefield();
    var gridElements = $('.grid-item');
    gridElements.each(function () {

        var element = $(this);
        var gameGrid = currentGrid[element.attr('x')][element.attr('y')];
        element.removeClass();
        element.addClass('grid-item');

        var done = checkStatus(game.getStatus());
        if (done === 'none') {
            if (gameGrid.state !== 'shown') {
                element.addClass('hidden');
                if (element.attr('flag') === 'true') {
                    element.addClass('flag');
                    numOfFlags += 1;
                }
            }
            else {
                if (gameGrid.state === 'shown') {
                    element.attr('flag', 'false');
                    element.addClass('shown');
                }
                if (gameGrid.count === 1) {
                    element.addClass('one')
                }
                else if (gameGrid.count === 2) {
                    element.addClass('two')
                }
                else if (gameGrid.count === 3) {
                    element.addClass('three')
                }
                else if (gameGrid.count === 4) {
                    element.addClass('four')
                }
                else if (gameGrid.count === 5) {
                    element.addClass('five')
                }
                else if (gameGrid.count === 6) {
                    element.addClass('six')
                }
                else if (gameGrid.count === 7) {
                    element.addClass('seven')
                }
                else if (gameGrid.count === 8) {
                    element.addClass('eight')
                }
            }
        }
        else {
            if (gameGrid.mine === true) {
                element.addClass('bomb')
            }
            else {
                element.addClass('shown')
            }
        }

        game.nmarked = numOfFlags;
    });

}

function badTimer(game, time) {
    clock = setInterval(function () {
        time += 1;
        if (checkStatus(game.getStatus()) === 'none') {
            $('#timer').text("Time: " + time + " sec");
        }
    }, 1000);
}

function startGame(difficulty) {
    var time = 0;

    clearTimeout(clock);
    $('#timer').text("Time: " + time + " sec");
    $(".grid-item").off();
    $("#winMessage").text("")

    let game = new MSGame();
    createGrid(game, difficulty);
    $('#flagCount').text("Flag Count: " + game.getStatus().nmarked);
    $('#mineNum').text("Mine Count: " + game.getStatus().nmines);

    $(".grid-item").on({
        "tap": function () {
            makeMove(game, $(this).attr('x'), $(this).attr('y'));
            winScreen(game.getStatus());
            updateGrid(game);
        },
        "contextmenu": function () {
            markFlag(game, $(this), $(this).attr('flag'));
            winScreen(game.getStatus());
            updateGrid(game);
            return false;
        },
        "taphold": function () {
            markFlag(game, $(this), $(this).attr('flag'));
            winScreen(game.getStatus());
            updateGrid(game);
            return false;
        },
    });

    badTimer(game, time);
}

var clock;
console.log("Starting here")
window.onload = function () {
    $.event.special.tap.emitTapOnTaphold = false;
    startGame('easy');
}

